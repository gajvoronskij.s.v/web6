function getPrices() {
    return {
        prodTypes: [100, 200, 300],
        prodOptions: {
            option1: 10,
            option2: 20,
            option3: 30
        },
        prodProperties: {
            prop1: 1,
            prop2: 2
        }
    };
}

function updatePrice() {
    let s = document.getElementsByName("prodType");
    let select = s[0];
    let price = 0;
    let prices = getPrices();
    let priceIndex = parseInt(select.value) - 1;
    if (priceIndex >= 0) {
        price = prices.prodTypes[priceIndex];
    }

    let radioDiv = document.getElementById("radios");
    radioDiv.style.display = (
        select.value === "2"
        ? "block"
        : "none"
    );

    if (select.value === "2") {
        let radios = document.getElementsByName("prodOptions");
        radios.forEach(function (radio) {
            if (radio.checked) {
                let optionPrice = prices.prodOptions[radio.value];
                price += optionPrice;
            }
        });
    }

    let checkDiv = document.getElementById("checkboxes");
    checkDiv.style.display = (
        select.value === "3"
        ? "block"
        : "none"
    );

    if (select.value === "3") {
        let checkboxes = document.querySelectorAll("#checkboxes input");
        checkboxes.forEach(function (checkbox) {
            if (checkbox.checked) {
                let propPrice = prices.prodProperties[checkbox.name];
                price += propPrice;
            }
        });
    }
    let amount = document.getElementById("amount");

    let prodPrice = document.getElementById("prodPrice");
    prodPrice.innerHTML = amount.value * price + " BTC";
}

window.addEventListener("DOMContentLoaded", function () {
    updatePrice();

    let radioDiv = document.getElementById("radios");
    radioDiv.style.display = "none";

    let s = document.getElementsByName("prodType");
    let select = s[0];

    select.addEventListener("change", function () {
        updatePrice();
    });

    let radios = document.getElementsByName("prodOptions");
    radios.forEach(function (radio) {
        radio.addEventListener("change", function () {
            updatePrice();
        });
    });

    let checkboxes = document.querySelectorAll("#checkboxes input");
    checkboxes.forEach(function (checkbox) {
        checkbox.addEventListener("change", function () {
            updatePrice();
        });
    });

    let amount = document.getElementById("amount");
    amount.addEventListener("input", function () {
        updatePrice();
    });

});